/**
 * Created by tianyang on 2017/4/3.
 */

/*
* 总结是传给helper method两个节点，cur和pre（最开始是head和null), 先用n1存当前节点的next，然后把当前节点的next指向pre，
* 然后一直recursively call help method直到过完整个linkedlist.
* */
public class ReverseLinkedListRecursive {
    public ListNode reverseList(ListNode head) {
        if(head == null||head.next== null)
            return head;
        return getReverse(head, null);
    }

    public ListNode getReverse(ListNode cur, ListNode prev){
        if(cur.next == null){
            cur.next = prev;
            return cur;
        }
        ListNode n1 = cur.next;
        cur.next = prev;
        return getReverse(n1,cur);
    }
}
