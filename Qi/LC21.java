// 21. Merge Two Sorted Lists
// Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

import java.util.*;

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
 }

public class LC21{

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) { //two pointer approach on linked list, time complexity O(n), my answer, too much unnecessary code
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        
        ListNode head;
        
        if(l1.val > l2.val){ //part1
            head = l2;
            l2 = l2.next;
        }else{
            head = l1;
            l1 = l1.next;
        }
        
        ListNode prev = head;
        while(l1!=null && l2!=null){ //part2
            if(l1.val > l2.val){
                prev.next = l2;
                l2 = l2.next;
            }else{
                prev.next = l1;
                l1 = l1.next;
            }
            prev = prev.next;
        } 
        
        if(l1 == null){
            prev.next = l2;
        }else if(l2 == null){
            prev.next = l1;
        }
        
        return head;
    }

    public ListNode mergeTwoLists2(ListNode l1, ListNode l2){ // this version of anwser use a fake head, and merge part1 and part2 into the same piece of code, concise coding, but not the natural process of thinking.
    	ListNode head = new ListNode(0);
        ListNode prev = head;
        while(l1!=null && l2!=null){
            if(l1.val > l2.val){
                prev.next = l2;
                l2 = l2.next;
            }else{
                prev.next = l1;
                l1 = l1.next;
            }
            prev = prev.next;
        }
        
        if(l1!=null){
            prev.next = l1;
        }else if(l2!=null){
            prev.next = l2;
        }
        
        return head.next;
    }



    public static void main(String[] args){
    	ListNode l1 = new ListNode(2);
    	ListNode l2 = new ListNode(3);
    	ListNode l3 = new ListNode(4);
    	ListNode l4 = new ListNode(5);
    	l1.next = l3;
    	l2.next = l4;

    	LC21 l = new LC21();
    	ListNode head = l.mergeTwoLists2(l1, l2);
    	while(head!=null){
    		System.out.println(head.val);
    		head = head.next;
    	}
    }

}
