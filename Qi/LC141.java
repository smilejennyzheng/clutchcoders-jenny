// 141. Linked List Cycle
// Given a linked list, determine if it has a cycle in it.

// Follow up:
// Can you solve it without using extra space?
import java.util.*;

class ListNode {
     int val;
     ListNode next;
     ListNode(int x) {
         val = x;
         next = null;
     }
}

public class LC141{
	public boolean hasCycle(ListNode head) {
        if(head == null){
            return false;
        }
        ListNode slow = head; // slow pointer runs 1 step ahead
        ListNode fast = head; // fast pointer runs 2 steps ahead
        
        while( fast.next!=null && fast.next.next!=null){ 
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast){ // if slow and fast pointers eventually run into each other, there must be a cycle
                return true;
            }
        }
        return false;
    }
}