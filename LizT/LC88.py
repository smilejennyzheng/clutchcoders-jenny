'''
88. Merge Sorted Array

Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.

Subscribe to see which companies asked this question.
'''
class Solution(object):
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        # nums1 = nums1[:m]
        while len(nums1)>m:
            nums1.pop()
        nums2 = nums2[:n]
        nums1.extend(nums2)
        
        ### sol 1, using python default function sort()
        # nums1.sort() 
        
        ### sol 2, using sorting 
        j = m+n
        while j > 0:
            for i in range(m+n-1, 0, -1):
                if nums1[i]<nums1[i-1]:
                    nums1[i-1], nums1[i] = nums1[i], nums1[i-1]
            j -=1