package LC94;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by lipingzhang on 3/31/17.
 */
public class Solution2 {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }

        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.isEmpty()) {
            pushAllLeft(root, stack);

            TreeNode top = stack.pop();;
            ans.add(top.val);
            root = top.right;
        }
        return ans;
    }


    private void pushAllLeft(TreeNode root, Stack<TreeNode> stack){
        while (root != null) {
            stack.push(root);
            root = root.left;
        }
    }
}
