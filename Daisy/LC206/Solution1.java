package LC206;

/**
 * Created by lipingzhang on 4/3/17.
 */
class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}

public class Solution1 {
    public ListNode reverseList(ListNode head) {
        if(head == null){
            return null;
        }

        ListNode dummy = new ListNode(0);
        while(head != null){
            ListNode next = head.next;
            head.next = dummy;
            dummy = head;
            head = next;
        }

        return dummy;
    }
}
