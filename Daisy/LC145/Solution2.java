package LC145;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lipingzhang on 3/31/17.
 */
public class Solution2 {
    public List<Integer> postorderTraversal(TreeNode root) {
        // use LinkedList
        LinkedList<Integer> ans = new LinkedList<Integer>();
        // can use Stack
        Deque<TreeNode> stack = new ArrayDeque<>();
        while(root != null || !stack.isEmpty()){
            if(root != null){
                stack.push(root);
                ans.addFirst(root.val);
                root = root.right;
            }else{
                root = stack.pop();
                root = root.left;
            }
        }
        return ans;
    }
}
