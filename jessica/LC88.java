public class Solution {
    public int[] retion(int[] input1, int[] input2) {
        Set<Integer> set = new HashSet<>();
        Set<Integer> ret = new HashSet<>();
        for (int i = 0; i < input1.length; i++) {
            set.add(input1[i]);
        }
        for (int i = 0; i < input2.length; i++) {
            if (set.contains(input2[i])) {
                ret.add(input2[i]);
            }
        }
        int[] result = new int[ret.size()];
        int i = 0;
        for (Integer num : ret) {
            result[i++] = num;
        }
        return result;
    }
}